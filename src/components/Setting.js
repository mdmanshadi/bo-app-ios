export const Setting = {
    APPKEY : 'OghiyaNoosEAbi1234567890',
    // BASEURL : 'http://10.0.2.2/bo/public/api/1/Base/',
    BASEURL : 'https://bo-app.liara.run/api/1/Base/',
    primaryColor : '#1b3281',
    secondColor : '#00aeef',
    ccc : '#535353',
    thirdColor : '#e6e7e8',
    headerColor : '#1b3281',
    white : '#fff',
    red : '#1b3281',
    green : '#1b3281',
    success : '#00aeef',
    f3 : '#f3f3f7',
    f9 : '#f9f9f9',
    grey : '#c2c2c2',
    contentMargin : 50,
    borderRadius : 7,
    bg : '#fff',
    sliderStyle : {
        width: '100%',
        height: 200,
    },
    flatListStyle : {
        marginTop:15,
        marginBottom:15
    }
};
export function numberFormat(x) {
    if(typeof x == 'undefined')
        return "";
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' تومان';
}
