import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert,WebView} from 'react-native';
import {connect, setStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import {Actions} from "react-native-router-flux";
import Screen from "./components/Screen";
import Header from "./components/Header";

export default class WebViewer extends React.Component {

    componentWillMount(){
        this.setState({
            loading:true,
        })
    }

    render() {
        var {height, width} = Dimensions.get('window');
        return(
            <View style={{marginTop:0,marginBottom:0,flex:1,}}>
                <Loading loading={this.state.loading}/>
                <Header title={this.props.title}/>
                <View style={{alignItems:'center',justifyContent:'center'}}>
                    <Image resizeMode={'stretch'} source={require('./../assets/images/bg/info.png')} style={{height:width/1.692,width:width}} />
                </View>
                <WebView
                    source={{uri: this.props.link}}
                    style={{width:width,flex:1,height:'100%'}}
                    onLoadStart={()=>this.setState({loading:true})}
                    onLoad={()=>this.setState({loading:false})}
                />
            </View>
        )

    }

}
