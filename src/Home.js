import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,AsyncStorage} from 'react-native';
import {connect, getStore, setStore} from "trim-redux";
import FaText from "./components/FaText";
import Swiper from 'react-native-swiper';
import {Setting} from "./components/Setting";
import Box from "./components/Box";
import Screen from "./components/Screen";
import {Actions} from "react-native-router-flux";



class Home extends React.Component {

    componentWillMount(){

        this.setState({
            sliders : this.props.homeData.sliders,
        });
    }
    componentDidMount(){
    }
    render() {
        var {height, width} = Dimensions.get('window');
        var homeData = this.props.homeData;

        return(

            <Screen home={true}  style={{margin:1,flex:1, backgroundColor: Setting.bg}} >


                {this.state.sliders.length > 0 &&
                <View style={{flex:1,height:265,shadowColor: "#ccc", shadowOffset: {width: 0, height:8,},shadowOpacity: 0.23, shadowRadius: 16.62,elevation: 5,backgroundColor:Setting.white}}>
                    <Swiper autoplay={true} autoplayTimeout={3} style={{width:width,height:260}}>
                        {this.state.sliders.map((val,i)=>{
                            return(
                                    <Image resizeMode={getStore('config').image_scale} style={{ width: '100%', height: '100%'}} source={{uri : val.pic}}/>
                            )
                        })}

                    </Swiper>
                </View>
                }


                {homeData.products.offer.length != 0 &&

                <View style={{marginRight:10,marginLeft:10,marginTop:20,marginBottom:20}}>
                        <View>
                            <View style={{flexDirection:'row-reverse',alignItems:'center',justifyContent:'center'}}>
                                <View style={{flex:2,flexDirection:'row-reverse'}}>
                                    <View style={{flex:1}}></View>
                                    <View style={{justifyContent:'flex-end',alignItems:'flex-end',flexDirection:'row-reverse'}}><FaText style={{marginRight:18,flex:2,fontSize:20}}>آخرین دوره ها</FaText></View>
                                    <View style={{flex:1}}></View>
                                </View>
                                <View style={{borderWidth:1,backgroundColor:Setting.headerColor,borderColor:Setting.headerColor,flex:3,height:1}}>
                                </View>
                            </View>
                            <FlatList
                                showsHorizontalScrollIndicator={false}

                                horizontal={true}
                                style={Setting.flatListStyle}
                                inverted={true}
                                data={homeData.products.offer}
                                renderItem={({item}) => <Box key={item.id} item={item}/>}
                                keyExtractor={(item)=>item.id}
                            />
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity  onPress={()=>Actions.replace('AllCourses')} style={{marginLeft:20,top:-10}}>
                                    <FaText>بیشتر ...</FaText>
                                </TouchableOpacity>
                            </View>
                        </View>
                </View>
                    }
                    {homeData.products.special.length != 0 &&
                        <View>
                            <View style={{flexDirection:'row-reverse',alignItems:'center',justifyContent:'center'}}>
                                <View style={{flex:6,flexDirection:'row-reverse'}}>
                                    <View style={{flex:1}}></View>
                                    <View style={{justifyContent:'flex-end',alignItems:'flex-end',flexDirection:'row-reverse'}}><FaText style={{marginRight:18,flex:2,fontSize:20}}>دوره های تخفیف دار</FaText></View>
                                    <View style={{flex:1}}></View>
                                </View>
                                <View style={{borderWidth:1,backgroundColor:Setting.headerColor,borderColor:Setting.headerColor,flex:5,height:1}}>
                                </View>
                            </View>
                            <FlatList
                                showsHorizontalScrollIndicator={false}

                                horizontal={true}
                                style={Setting.flatListStyle}
                                inverted={true}
                                data={homeData.products.special}
                                renderItem={({item}) => <Box key={item.id} item={item}/>}
                                keyExtractor={(item)=>item.id}
                            />
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity  onPress={()=>Actions.replace('AllCourses')} style={{marginLeft:20,top:-10}}>
                                    <FaText>بیشتر ...</FaText>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }

                    {homeData.products.category.length != 0 &&
                        <View>
                            <View style={{flexDirection:'row-reverse',alignItems:'center',justifyContent:'center'}}>
                                <View style={{flex:2,flexDirection:'row-reverse'}}>
                                    <View style={{flex:1}}></View>
                                    <View style={{justifyContent:'flex-end',alignItems:'flex-end',flexDirection:'row-reverse'}}><FaText style={{marginRight:18,flex:2,fontSize:20}}>آخرین پکیج ها</FaText></View>
                                    <View style={{flex:1}}></View>
                                </View>
                                <View style={{borderWidth:1,backgroundColor:Setting.headerColor,borderColor:Setting.headerColor,flex:3,height:1}}>
                                </View>
                            </View>
                            <FlatList
                                showsHorizontalScrollIndicator={false}

                                horizontal={true}
                                style={Setting.flatListStyle}
                                inverted={true}
                                data={homeData.products.category}
                                renderItem={({item}) => <Box package={true} key={item.id} item={item}/>}
                                keyExtractor={(item)=>item.id}
                            />
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity  onPress={()=>Actions.replace('AllCourses')} style={{marginLeft:20,top:-10}}>
                                    <FaText>بیشتر ...</FaText>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }


            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData
    }
};

export default connect(mapStateToProps)(Home);
