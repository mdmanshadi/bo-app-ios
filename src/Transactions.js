import React from 'react';
import {View, Image, Dimensions, TouchableOpacity, FlatList, Platform} from 'react-native';
import FaText from "./components/FaText";
import {Fetcher} from "./components/Upload";
import Loading from "./components/Loading";
import Screen from "./components/Screen";
import {Actions} from "react-native-router-flux";
import {Setting} from "./components/Setting";

export default class Transactions extends React.Component {


    componentWillMount(){
        this.setState({
            data : [],
            loading : false,
        });

        this.getData();
    }
    async getData(){
        var data = await Fetcher("userTransactionHistory",{},true);
        if(data.status == "OK"){
            this.setState({
                data : data.data,
                loading : false,
            })
        }else{
            this.setState({loading:false});
            setTimeout(()=>Actions.pop(),100);
        }
    }

    render() {
        var {height, width} = Dimensions.get('window');

        if(this.state.data.length == 0)
            return(
                <Screen float={true}  alignItems={'center'} title={"گردش حساب"}>
                    <Loading new loading={this.state.loading}/>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/transaction.png')} style={{height:width/1.432,width:width,marginBottom:30}} />
                        <View style={{backgroundColor:Setting.red,flexDirection:'row-reverse',alignItems:'center',justifyContent:'center',padding:10}}>
                            <Image source={require('../assets/images/drawer/error.png')} style={{width:30,height:30,marginLeft:8}}/>
                            <FaText style={{color:'#fff',fontSize:12}}>تاکنون گردش حسابی نداشته اید.</FaText>
                        </View>

                    </View>
                </Screen>
            )
        else
            return(
                <Screen float={true} alignItems={'center'} title={"گردش حساب"}>
                    <Loading new loading={this.state.loading}/>
                    <View style={{alignItems:'center',justifyContent:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/transaction.png')} style={{height:width/1.432,width:width}} />
                    </View>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:30,marginBottom:20,justifyContent:'center'}}>

                        <FlatList
                        data={this.state.data}
                        renderItem={({item}) =>
                            <View style={{borderColor:'#e9e9e9',borderRadius:0,margin:5,width:'98%',backgroundColor:'#fff',borderWidth:1}}>
                                <View style={{padding:10,justifyContent:'center',alignItems:'center',backgroundColor:Setting.f9}}>
                                    <FaText style={{color:Setting.primaryColor}}>{item.created}</FaText>

                                    <FaText style={{marginTop:15}}>{item.title}</FaText>
                                    <FaText>مبلغ : {item.amount} تومان</FaText>
                                    <FaText style={{marginTop:15}}>{item.description}</FaText>
                                    <FaText style={{fontSize:10,color:'#848484'}}> توضیحات : {item.res_id}</FaText>
                                </View>
                            </View>
                        }
                        keyExtractor={item => item.id}
                    />
                    </View>
                </Screen>
            )
    }
}