import React from 'react';
import {AsyncStorage, Image,View} from "react-native";
import FaText from "./components/FaText";
import {Icon,Button} from "native-base";
import MyButton from "./components/MyButton";
import {Actions} from 'react-native-router-flux';
import {Setting} from "./components/Setting";
export default class NoNet extends React.Component {
    componentWillMount(){
    }
    render() {
        return (
            <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}>
                <View style={{marginTop:'25%',flex:1,width:'60%'}}>
                    <Icon type={"FontAwesome"} name="wifi" style={{fontSize:150,textAlign:'center',color:Setting.primaryColor}}/>
                    <FaText style={{fontSize:25,textAlign:'center',marginTop:'10%'}}>اتصال شبکه برقرار نیست</FaText>
                    <FaText style={{fontSize:14,textAlign:'center'}}>لطفا از اتصال خود به اینترنت مطمین شوید</FaText>
                    <MyButton style={{position: 'absolute',bottom:20,width:'100%'}} onPress={()=>Actions.reset('splash')}>سعی مجدد</MyButton>
                </View>
            </View>
        )
    }
}
