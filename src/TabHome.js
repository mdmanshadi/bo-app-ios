import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity} from 'react-native';
import {connect, setStore} from "trim-redux";
import FaText from "./components/FaText";
import Swiper from 'react-native-swiper';
import {Setting} from "./components/Setting";
import Box from "./components/Box";
import Header from "./components/Header";
import {Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';
import Screen from "./components/Screen";



class Home extends React.Component {

    componentWillMount(){
        //console.log(this.props.homeData);
        this.setState({
            sliders : this.props.homeData.sliders
        })
    }
    render() {
        var {height, width} = Dimensions.get('window');
        var homeData = this.props.homeData;

        return(

            <Screen home={true}  style={{margin:1,flex:1, backgroundColor: Setting.bg}} >

                {this.state.sliders.length > 0 &&
                    <View style={{flex:1}}>
                    <Swiper autoplay={true} autoplayTimeout={3} style={{width:width,height:230}}>
                        {this.state.sliders.map((val,i)=>{
                            return(
                                <Image resizeMode={'stretch'} style={{ width: '100%', height: '100%'}} source={{uri : val.pic}}/>
                            )
                        })}

                    </Swiper>
                    </View>
                }


                <View style={{marginRight:10,marginLeft:10,marginTop:10,marginBottom:20}}>
                    <FaText>آخرین دوره ها</FaText>
                    <FlatList
                        horizontal={true}
                        style={Setting.flatListStyle}
                        inverted={true}
                        data={homeData.products.offer}
                        renderItem={({item}) => <Box key={item.id} item={item}/>}
                        keyExtractor={(item)=>item.id}
                    />
                    <FaText>دوره های تخفیف دار</FaText>
                    <FlatList
                        horizontal={true}
                        style={Setting.flatListStyle}
                        inverted={true}
                        data={homeData.products.special}
                        renderItem={({item}) => <Box key={item.id} item={item}/>}
                        keyExtractor={(item)=>item.id}
                    />
                    <FaText>پکیج ها</FaText>
                    <FlatList
                        horizontal={true}
                        style={Setting.flatListStyle}
                        inverted={true}
                        data={homeData.products.category}
                        renderItem={({item}) => <Box package={true} key={item.id} item={item}/>}
                        keyExtractor={(item)=>item.id}
                    />

                </View>


            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData
    }
};

export default connect(mapStateToProps)(Home);
